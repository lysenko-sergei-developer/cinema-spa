import React, { Component } from 'react'

import Film from './Film'
import FilmForm from './FilmForm' 
import { ControlPanel } from './ControlPanel'

import { uniqueID, stringToArray, sortFilmsByTitle,
         observerFilms, translator } from '../utils'

import axios from 'axios' 

class Dashboard extends Component {
  constructor () {
    super()

    this.state = {
      films: [],
      curFilms: [],
      curTitle: '',
      curActor: ''
    }
  }

  componentWillMount = () => {
    this.initializeFilms()
  }

  initializeFilms = () => {
    const API = 'https://cinema-api.herokuapp.com/'
    axios.get(API+'api/films')
      .then(response => this.setState ({ films: response.data, curFilms: response.data }) )
      .catch(req => console.log(req) )
  }

  //TODO: REWRITE TO DRY
  searchTitle = (string) => {
    if(this.state.films.length === 0) return

    let nextFilms
    nextFilms = this.state.films.slice()
    nextFilms = observerFilms(string, 'title', nextFilms)
    nextFilms = observerFilms(this.state.curActor, 'actor', nextFilms)

    this.setState({
      curFilms: nextFilms,
      curTitle: string
    })

  }

  uploadText = (e) => {
    let file = e.target.files[0]
    let types = 'text/plain'
    if (!file || !types.includes(file.type)) {
      e.target.value = null
      throw new TypeError('Wrong type of file')
    }

    let reader = new FileReader()
    reader.onload = () => {
      const nextFilms = translator (reader.result)

      this.setState ({
        films: nextFilms,
        curFilms: nextFilms
      })
    }
    reader.readAsText(file)
  }

  //TODO: REWRITE TO DRY
  searchActor = (string) => {
    if(this.state.films.length === 0) return

    let nextFilms
    nextFilms = this.state.films.slice()
    nextFilms = observerFilms(this.state.curTitle, 'title', nextFilms)
    nextFilms = observerFilms(string, 'actor', nextFilms)

    this.setState({
      curFilms: nextFilms,
      curActor: string
    })

  }

  sortFilms = () => {
    let temp = this.state.curFilms || this.state.films.slice()
    let sort = this.state.isSort
    let nextFilms

    if (sort) {
      nextFilms = sortFilmsByTitle(temp, -1)
      this.setState ({ isSort: false})
    } else if (!sort) {
      nextFilms = sortFilmsByTitle(temp, 1)
      this.setState ({ isSort: true}) 
    }
    
    this.setState ({
      films: nextFilms,
      curFilms: nextFilms 
    })
  }

  addFilm = (event, filmState) => {
    function addFilmById (filmState) {
      const API = 'https://cinema-api.herokuapp.com/'

      axios.post(API+'api/films', filmState)
        .then(response => response.status === 201)
        .catch(req => 'error '+req.response.data.errors[0])
    }

    event.preventDefault()

    const film = {
      id: uniqueID(),
      title: filmState.title,
      release: filmState.release,
      format: filmState.format,
      stars: stringToArray( filmState.stars )
    }

    addFilmById (film)

    let nextFilms = this.state.films
    nextFilms.push(film)

    this.setState ({ 
      curFilms: nextFilms,
      films: nextFilms
    }) 
  }

  deleteFilm = (id) => {
    function deleteFilmByID (id) {
      const API = 'https://cinema-api.herokuapp.com/'

      axios.post(API+'api/films/'+id)
        .then(response => response.status === 201)
        .catch(req => 'error '+req.response.data.errors[0])
    }

    deleteFilmByID (id)

    let nextFilms = this.state.films.filter((film) => {
      if (film.id !== id) return film
    })

    this.setState ({ films: nextFilms })

    if (this.state.curTitle !== '') {
      nextFilms = observerFilms(this.state.curTitle, 'title', nextFilms)
    }

    if (this.state.curActor !== '') {
      nextFilms = observerFilms(this.state.curActor, 'actor', nextFilms)
    }

    this.setState ({ 
      curFilms: nextFilms
    })
  }

  render () {
    const films = this.state.curFilms

    return (
      <div className="Dashboard">
        <FilmForm 
          addFilm={this.addFilm}
        />
        
        <ControlPanel 
          sortFilms={this.sortFilms}
          searchTitle={this.searchTitle}
          searchActor={this.searchActor}
          uploadText={this.uploadText}
        />
        
        <table className="film-table">
          <tbody>
            <tr>
              <th>Title</th>
              <th>Release Year</th>
              <th>Format</th>
              <th>Stars</th>
            </tr>
            {films.map( (film, index) => 
              <Film key={uniqueID()} 
                    deleteFilm={this.deleteFilm}
                    backgroundTable={index % 2 ? 'table-gray' : 'table-white'}
                    {...film}
              />
            )}
          </tbody>
        </table>
      </div>
    )
  } 
}

export default Dashboard