import React, { Component } from 'react'

import { uniqueID } from '../utils'

class Film extends Component {
  constructor (props) {
    super(props)

    this.state = {
      id: props.id || uniqueID(),
      title: props.title || '',
      release: props.release || '',
      format: props.format || '',
      stars: props.stars || []
    }
  }

  render () {
    return (
            <tr className={"film-cell "+this.props.backgroundTable}>
              <td className="title">{this.state.title}</td>
              <td className="release">{this.state.release}</td>
              <td className="format">{this.state.format}</td>
              <td className="stars">
                <ul className="star-list">
                {this.state.stars.map( star => 
                  <li className="star-item"
                      key={uniqueID()}>{star}</li>)
                }
                </ul>
              </td>
              <td className="control-panel">
                <span className="delete" onClick={() => this.props.deleteFilm(this.state.id)}>X</span>
              </td>
            </tr>
    )
  } 
}

export default Film