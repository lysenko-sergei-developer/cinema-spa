import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { validateString } from '../utils'

export default class FilmForm extends Component {
  constructor (props) {
    super(props)

    this.state = {
      title: props.title || '',
      release: props.release || '',
      format: props.format || '',
      stars: props.stars || []
    }
  }

  handleTitle = (newTitle) => this.setState ({ title: newTitle.target.value })

  handleRelease = (newRelease) => this.setState ({ release: newRelease.target.value })

  handleFormat = (newFormat) => this.setState ({ format: newFormat.target.value })

  handleStars = (newStars) => this.setState ({ stars: newStars.target.value }) 

  handleSendForm = (event) => {
    event.preventDefault()

    if (this.state.title === '' ||
        this.state.release === '' ||
        this.state.format === '' ||
        this.state.stars === '') {
          return
        } 

    const film = {
      title: validateString(this.state.title),
      release: validateString(this.state.release),
      format: validateString(this.state.format),
      stars: validateString(this.state.stars)
    }

    this.props.addFilm(event, film)
    
    this.setState ({
      title: '',
      release: '',
      format: '',
      stars: ''
    })
  }

  render() {
    return (
    <form className="film-form">
      <div className="title-form">
        <p className="title">Title: </p>
        <input type="text" onChange={this.handleTitle} value={this.state.title}/>
      </div>

      <div className="year-form">
        <p className="year">Release Year: </p>
        <input type="text" onChange={this.handleRelease} value={this.state.release}/>
      </div>

      <div className="format-form">
        <p className="format">Format: </p>
        
        <span className="format-val"><input type="radio" name="format" value="VHS" onChange={this.handleFormat}/>VHS</span>
        <span className="format-val"><input type="radio" name="format" value="DVD" onChange={this.handleFormat}/>DVD</span>
        <span className="format-val"><input type="radio" name="format" value="Blu-Ray" onChange={this.handleFormat}/>Blu-Ray</span>
      </div>

      <div className="stars-form">
        <p className="stars">Stars: </p>
        <textarea onChange={this.handleStars} value={this.state.stars}></textarea>
      </div>

      <button className="add-film-button" onClick={this.handleSendForm}>Add film</button>
    </form>
    )
  }
}

FilmForm.PropTypes = {
  title: PropTypes.string.isRequired,
  release: PropTypes.string.isRequired,
  format: PropTypes.string.isRequired,
  stars: PropTypes.array.isRequired,
}