import React from 'react'

export const ControlPanel = (props) => {

  return (
    <div className="control-panel">
      <Sort sortFilms={props.sortFilms} />
      <SearchByTitle searchTitle={props.searchTitle} />
      <SearchByActor searchActor={props.searchActor}/>
      <UploadFile uploadText={props.uploadText}/>
    </div>
  )
}

const Sort = (props) => {
  function handleSort (e) {
    props.sortFilms()
  }
  return <span>Sort: <span onClick={handleSort}>A-Z</span></span>
}

const SearchByTitle = (props) => {
  function handleSearchByTitle (e) {
    props.searchTitle(e.target.value)
  }
  return (
    <span>Search by title: 
      <input type="text" onChange={handleSearchByTitle}/>
    </span>
  )
}

const SearchByActor = (props) => {
  function handleSearchByActor (e) {
    props.searchActor(e.target.value)
  }
  return (
    <span>Search by actor:
      <input type="text" onChange={handleSearchByActor}/>
    </span>
  )
}

const UploadFile = (props) => {
  function handleUploadFile (e) {
    props.uploadText(e)
  }
  return (
    <div className="upload-text">
      <input type="file" id="upload-film-list" onChange={handleUploadFile}
             accept="plain/text"/>
      <label htmlFor="upload-film-list">Upload text</label>
    </div>
  )
}

