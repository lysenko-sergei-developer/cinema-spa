export const sortFilmsByTitle = (films, higher) => {  
  const newFilms = films.sort( (cur, next) => {
    if (cur.title > next.title) return higher
    if (cur.title < next.title) return -higher
    return 0
  })

  return newFilms
}

const searchByTitle = (string, films) => {
  const newFilms = films.filter( (cur) => {
    if (cur.title.includes(string)) return cur
  })
  return newFilms
}

const searchByActor = (string, films) => {
    let curObject,
        isIncludes

    // FIX THIS BUG WITH BOOLEAN VARIABLE
    const newFilms = films.filter( (cur) => {
      isIncludes = false
      cur.stars.forEach( (actor, i) => {
        curObject = actor.includes(string) ? cur : curObject
        actor.includes(string) === true ? isIncludes = true : isIncludes
      })
      
      if (isIncludes) return curObject
    })
    
    return newFilms
}

export const observerFilms = (string, type, curFilms) => {
  let nextFilms

  switch(type) {
    case 'title':
      nextFilms = searchByTitle(string, curFilms)
    break

    case 'actor':
      nextFilms = searchByActor(string, curFilms)
    break

    default:
      console.log(type)
    break
  }

  return nextFilms
}