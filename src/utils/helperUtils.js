export const uniqueID = () => {
  let text = "";
  let length = 8
  const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (let i = 0; i < length; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
} 

export const stringToArray = (string, symb = ',') => string.split(symb)

export const validateString = (string) => {
  let nextString = string.trim()

  return nextString
}
