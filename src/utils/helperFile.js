import {
  stringToArray,
  uniqueID
} from './'

export const translator = (text) => {
  let temp
  let films = []

  temp = text.split('\n')

  let typeSymbol,
    type,
    data

  let film = {
    id: uniqueID(),
    title: '',
    release: '',
    format: '',
    stars: []
  }

  temp.forEach((string) => {
    if (string === '') return
    
    typeSymbol = string.match(':')
    type = string.substring(0, typeSymbol.index)
    data = string.substring(typeSymbol.index+2, string.length)

    switch (type) {
      case 'Title':
        film.title = data
        break

      case 'Release Year':
        film.release = data
        break

      case 'Format':
        film.format = data
        break

      case 'Stars':
        const arr = stringToArray(data, ', ')
        film.stars = arr
        break

      default:
        console.log('get type')
    }

     if (film.title.length !== 0 &&
      film.release.length !== 0 &&
      film.format.length !== 0 &&
      film.stars.length !== 0) {
      
      let filmCopy = Object.assign({}, film) 
      films.push(filmCopy)

      film.id = uniqueID()
      film.title = ''
      film.release = ''
      film.format = ''
      film.stars = []
    }
  })
  
  return films
}