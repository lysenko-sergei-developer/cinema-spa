import React, { Component } from 'react';

import Dashbord from './components/Dashboard'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Dashbord />
      </div>
    );
  }
}

export default App;
