# Инструкция к запуску Cinema for Webbylab SPA

1. Если у вас нету git скачайте git https://git-scm.com/
2. Запустите git и выполните команду git clone git@github.com:lysenko-sergei-developer/cinema-spa.git
3. Перейдите в директорию cinema-spa 
4. Если у вас нету npm https://www.npmjs.com/get-npm
5. Запустите команду npm i
6. Запустите команду npm start
7. Откройте приложение по адресу http://localhost:3000